__kernel void __attribute__ ((reqd_work_group_size(15,1,1))) fire10(__global float *fire10_Weights_hw, __global float *fire9_Features,  __global float *fire10_Features)
		{
	int x = get_local_id(0);
	int y = get_group_id(0);

	float Features = 0;
	if(x!=0 && x!=14 && y!=0 && y!=14)
	{
		for(int f=0; f<1000; f++)
		{
			Features = 0;
			for(int n=0; n<512; n++)
			{
				Features+= fire9_Features[n*13*13 + (x-1)*13 + y-1]*fire10_Weights_hw[f*512+n];
			}
			//ReLU activation function computation
			if(Features<0)
				Features = 0;
			fire10_Features[f*15*15 + x*15 + y] = Features;// + fire8squeeze1x1_Weights_GPU[24576 + f];
			//printf("%.8f ",Features);
		}
	}


}
